# Sources

[eduVPN Artwork](https://github.com/eduvpn/artwork)

# OS Logos

We took the OS logos from Wikipedia and converted them to 128x128 PNG files
using [ImageMagick](https://imagemagick.org/) and 
[GIMP](https://www.gimp.org/).

For the dark versions, we used GIMP to invert the colors of the macOS and iOS logos.

- [Windows](https://en.wikipedia.org/wiki/Microsoft_Windows)
- [Linux](https://en.wikipedia.org/wiki/Linux)
- [Android](https://en.wikipedia.org/wiki/Android_(operating_system))
- [macOS](https://en.wikipedia.org/wiki/MacOS)
- [iOS](https://en.wikipedia.org/wiki/IOS)
