<?php

declare(strict_types=1);

/*
 * eduVPN - End-user friendly VPN.
 *
 * Copyright: 2014-2023, The Commons Conservancy eduVPN Programme
 * SPDX-License-Identifier: AGPL-3.0+
 */

return [
    'Android' => 'Android',
    'If you do not want to, or cannot use the official eduVPN apps, you can also manually obtain a VPN configuration and import it in your existing VPN application.' => 'Si vous ne souhaitez pas ou ne pouvez pas utiliser les applications eduVPN officielles, vous pouvez également obtenir une configuration VPN et l\'importer dans votre application VPN existante',
    'Linux' => 'Linux',
    'Manual Configuration' => 'Configuration manuelle',
    'On the "Account" page you can block access to the VPN in case you lose a device, or no longer use the VPN.' => 'Sur la page "Compte", vous pouvez bloquer l\'accès au VPN si vous avez perdu un appareil ou ne souhaitez plus utiliser un VPN',
    'To use eduVPN, download the app for your device below!' => 'Pour utiliser eduVPN, téléchargez l\'application correspondant à votre appareil ci-dessous !',
    'Welcome to eduVPN!' => 'Bienvenue sur eduVPN !',
    'Windows' => 'Windows',
    'iOS' => 'iOS',
    'macOS' => 'macOS',
];
