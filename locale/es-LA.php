<?php

declare(strict_types=1);

/*
 * eduVPN - End-user friendly VPN.
 *
 * Copyright: 2014-2023, The Commons Conservancy eduVPN Programme
 * SPDX-License-Identifier: AGPL-3.0+
 */

return [
    'Android' => 'Android',
    'If you do not want to, or cannot use the official eduVPN apps, you can also manually obtain a VPN configuration and import it in your existing VPN application.' => 'Si no desea o no puede utilizar las aplicaciones oficiales de eduVPN, también puede obtener manualmente una configuración de VPN e importarla en su aplicación VPN existente.',
    'Linux' => 'Linux',
    'Manual Configuration' => 'Configuración Manual',
    'On the "Account" page you can block access to the VPN in case you lose a device, or no longer use the VPN.' => 'En la página "Cuenta" puede bloquear el acceso a la VPN en caso de que pierda un dispositivo o ya no use la VPN.',
    'To use eduVPN, download the app for your device below!' => '¡Para usar eduVPN, descargue la aplicación para su dispositivo a continuación!',
    'Welcome to eduVPN!' => '¡Bienvenido a eduVPN!',
    'Windows' => 'Windows',
    'iOS' => 'iOS',
    'macOS' => 'macOS',
];
