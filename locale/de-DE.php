<?php

declare(strict_types=1);

/*
 * eduVPN - End-user friendly VPN.
 *
 * Copyright: 2014-2023, The Commons Conservancy eduVPN Programme
 * SPDX-License-Identifier: AGPL-3.0+
 */

return [
    'Android' => 'Android',
    //'If you do not want to, or cannot use the official eduVPN apps, you can also manually obtain a VPN configuration and import it in your existing VPN application.' => '',
    'Linux' => 'Linux',
    //'Manual Configuration' => '',
    //'On the "Account" page you can block access to the VPN in case you lose a device, or no longer use the VPN.' => '',
    'To use eduVPN, download the app for your device below!' => 'Um eduVPN zu nutzen lade dir die App für dein Gerät herunter!',
    'Welcome to eduVPN!' => 'Willkommen bei eduVPN!',
    'Windows' => 'Windows',
    'iOS' => 'iOS',
    'macOS' => 'macOS',
];
