<?php

declare(strict_types=1);

/*
 * eduVPN - End-user friendly VPN.
 *
 * Copyright: 2014-2023, The Commons Conservancy eduVPN Programme
 * SPDX-License-Identifier: AGPL-3.0+
 */

return [
    'Android' => 'اندرويد',
    'If you do not want to, or cannot use the official eduVPN apps, you can also manually obtain a VPN configuration and import it in your existing VPN application.' => 'اذا لا تريد استخدام التطبيقات الرسمية، يمكنك الحصول على اعداد للشبكة الخاصة الافتراضية واستخدامه في تطبيقك المفضل.',
    'Linux' => 'لينكس',
    'Manual Configuration' => 'اعداد يدوي',
    'On the "Account" page you can block access to the VPN in case you lose a device, or no longer use the VPN.' => 'يمكن منع الولوج الى الشبكة الخاصة الافتراضية على صفحة "حساب"',
    'To use eduVPN, download the app for your device below!' => 'لاستعمال eduVPN، حمل التطبيق على جهازك أسفله!',
    'Welcome to eduVPN!' => 'مرحبا بك على eduVPN!',
    'Windows' => 'ويندوز',
    'iOS' => 'آي أو إس',
    'macOS' => 'ماك',
];
