# Credits

* `ar-MA`: Sami Ait Ali Oulahcen (MARWAN)
* `cs-CZ`: Roman Mariancik, Ivo Durnik, and Michal Drobny (ICS MUNI, Czech Republic)
* `da-DK`: Mikkel Hald (DeiC)
* `de-DE`: Fred-Oliver Jury (University of applied Sciences Osnabrück)
* `et-EE`: Anne Märdimäe (EENet of HITSA)
* `fr-FR`: Tangui Coulouarn (DeiC)
* `nb-NO`: Jørn Åne de Jong (Uninett AS)
* `nl-NL`: François Kooman (DeiC)
* `pt-PT`: Raul Ferreira, Marco Teixeira (Universidade do Minho)
* `sk-SK`: Roman Mariancik, Ivo Durnik, and Michal Drobny (ICS MUNI, Czech Republic)
