<?php

declare(strict_types=1);

/*
 * eduVPN - End-user friendly VPN.
 *
 * Copyright: 2014-2023, The Commons Conservancy eduVPN Programme
 * SPDX-License-Identifier: AGPL-3.0+
 */

return [
    'Android' => 'Android',
    'If you do not want to, or cannot use the official eduVPN apps, you can also manually obtain a VPN configuration and import it in your existing VPN application.' => 'Si no deseas o no puedes utilizar las aplicaciones oficiales de eduVPN, también puedes obtener manualmente una configuración de VPN e importarla en tu aplicación VPN existente.',
    'Linux' => 'Linux',
    'Manual Configuration' => 'Configuración manual',
    'On the "Account" page you can block access to the VPN in case you lose a device, or no longer use the VPN.' => 'En la página "Cuenta" puedes bloquear el acceso a la VPN en caso de que pierdas un dispositivo o ya no uses la VPN.',
    'To use eduVPN, download the app for your device below!' => '¡Para usar eduVPN, descarga la aplicación para tu dispositivo a continuación!',
    'Welcome to eduVPN!' => '¡Bienvenido a eduVPN!',
    'Windows' => 'Windows',
    'iOS' => 'iOS',
    'macOS' => 'macOS',
];
