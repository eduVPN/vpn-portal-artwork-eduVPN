<?php

declare(strict_types=1);

/*
 * eduVPN - End-user friendly VPN.
 *
 * Copyright: 2014-2023, The Commons Conservancy eduVPN Programme
 * SPDX-License-Identifier: AGPL-3.0+
 */

return [
    'Android' => 'Android',
    'If you do not want to, or cannot use the official eduVPN apps, you can also manually obtain a VPN configuration and import it in your existing VPN application.' => 'Hvis du ikke kan eller ikke ønsker at bruge de officielle eduVPN-apps, kan du i stedet få en VPN-konfiguration manuelt og importere den i den VPN-applikation du ellers bruger.',
    'Linux' => 'Linux',
    'Manual Configuration' => 'Manuel opsætning',
    'On the "Account" page you can block access to the VPN in case you lose a device, or no longer use the VPN.' => 'På "Konto"-siden kan du blokere adgang til VPN\'en hvis du mister en enhed eller ikke længere bruger VPN\'en.',
    'To use eduVPN, download the app for your device below!' => 'Kom i gang med eduVPN ved at downloade app\'en til din enhed nedenfor!',
    'Welcome to eduVPN!' => 'Velkommen til eduVPN!',
    'Windows' => 'Windows',
    'iOS' => 'iOS',
    'macOS' => 'macOS',
];
