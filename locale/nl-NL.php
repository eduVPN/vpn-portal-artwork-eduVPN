<?php

declare(strict_types=1);

/*
 * eduVPN - End-user friendly VPN.
 *
 * Copyright: 2014-2023, The Commons Conservancy eduVPN Programme
 * SPDX-License-Identifier: AGPL-3.0+
 */

return [
    'Android' => 'Android',
    'If you do not want to, or cannot use the official eduVPN apps, you can also manually obtain a VPN configuration and import it in your existing VPN application.' => 'Als je geen gebruikt wilt of kunt maken van de officiële eduVPN apps, kun je ook handmatig een VPN-configuratie bemachtigen en in je bestaande VPN-applicatie importeren.',
    'Linux' => 'Linux',
    'Manual Configuration' => 'Handmatige configuratie',
    'On the "Account" page you can block access to the VPN in case you lose a device, or no longer use the VPN.' => 'Op de "Account" pagina kun je toegang tot het VPN blokkeren als je je device verliest, of het VPN niet langer gebruikt.',
    'To use eduVPN, download the app for your device below!' => 'Om eduVPN te gebruiken kun je de app voor jouw device hieronder downloaden!',
    'Welcome to eduVPN!' => 'Welkom bij eduVPN!',
    'Windows' => 'Windows',
    'iOS' => 'iOS',
    'macOS' => 'macOS',
];
