# Changelog

## 3.0.6 (2024-11-01)
- implement dark mode for eduVPN 
  ([#5](https://codeberg.org/eduVPN/vpn-portal-artwork-eduVPN/pulls/5))

## 3.0.5 (2024-06-10)
- update `ar-MA` translation
- update `fr-FR` translation

## 3.0.4 (2024-05-13)
- add `da-DK` translation
- add `es-ES` translation
- add `es-LA` translation

## 3.0.3 (2024-02-15)
- remove `<details>` for manual configuration downloads
- update `cs-CZ` translation

## 3.0.2 (2023-10-09)
- update OS logos
- update link to Linux client (documentation)

## 3.0.1 (2023-09-19)
- add `cs-CZ` translation
- add `sk-SK` translation

## 3.0.0 (2022-05-17)
- update templates for 3.x

## 2.2.3 (2020-09-08)
- reduce padding of <nav>
- fix CSS cache busting

## 2.2.2 (2020-07-27)
- small fix for base theme update

## 2.2.1 (2020-07-10)
- update `de_DE` translation
- add `nb_NO` translation
- add `pl_PL` translation
- add `uk_UA` translation
- add `fr_FR` translation
- update `ar_MA` translation
- update icons to look better on high resolution screens
- prevent menu from flowing over the logo

## 2.2.0 (2020-06-29)
- implement new design

## 2.1.4 (2020-04-11)
- add Portuguese translation
- remove GEANT contact info from Documentation page

## 2.1.3 (2020-03-23)
- add German translation

## 2.1.2 (2020-01-27)
- point the Mac App Store for macOS application

## 2.1.1 (2020-01-20)
- add Estionian translation
- size the profile select box to the size of the number of items, auto 
  selecting the first if there is only one

## 2.1.0 (2019-11-04)
- update for new portal UI templates
- add eduVPN theme translations
- update "Home" screen to display application download links
- have a eduVPN specific documentation page
- initially hide the manual configuration creation, only for advanced users...

## 2.0.2 (2019-08-20)
- change the color for the graphs, hurts the eyes less

## 2.0.1 (2019-08-19)
- update for new HTML/CSS stats

## 2.0.0 (2019-04-01)
- update for PHP templates
- add configuration file

## 1.3.0 (2018-11-28)
- implement SAML logout support in `base.twig` template

## 1.2.3 (2018-09-10)
- use "Certificates" instead of "Configurations"

## 1.2.2 (2018-05-24)
- support "customFooter"

## 1.2.1 (2018-01-23)
- update color of stylesheet to match color in updated logo

## 1.2.0 (2018-01-22)
- update eduVPN logo

## 1.1.0 (2018-01-02)
- initial release
